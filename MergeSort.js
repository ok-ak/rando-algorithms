const numbers = [3, 2, 8, 4, 1, 9, 0, 5, 7, 6];

const mergeSort = (arr) => {
  if(!arr || arr.length === 0){
    return [];
  }

  if(arr.length === 1){
    return arr;
  }

  const midIndex = arr.length/2;
  const arrA = arr.slice(0, midIndex);
  const arrB = arr.slice(midIndex, arr.length);

  console.log('arrA', arrA);
  console.log('arrB', arrB);

  const sortedArrA = mergeSort(arrA);
  const sortedArrB = mergeSort(arrB);

  let mergedResult = [];
  let count = 0;
  console.log('sortedArrA', sortedArrA);
  console.log('sortedArrB', sortedArrB);

  while(count <= arr.length){
    if(sortedArrA.length < 1){
      mergedResult = [...mergedResult, ...sortedArrB];
      break;
    } else if(sortedArrB.length < 1){
      mergedResult = [...mergedResult, ...sortedArrA];
      break;
    } else if(sortedArrA[0] < sortedArrB[0]){
      mergedResult.push(sortedArrA.shift());
    } else {
      mergedResult.push(sortedArrB.shift());
    }

    count += 1;
  }

  return mergedResult;
}

console.log('MergeSort:', mergeSort(numbers));